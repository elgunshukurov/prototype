package az.ingress.prototype;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrototypeApplication{

	public static void main(String[] args) {
		SpringApplication.run(PrototypeApplication.class, args);
	}

}
