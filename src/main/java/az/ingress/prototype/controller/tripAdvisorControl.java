package az.ingress.prototype.controller;

import az.ingress.prototype.domain.Traveller;
import az.ingress.prototype.dto.TravellerDto;
import az.ingress.prototype.dto.TravellerRequestDto;
import az.ingress.prototype.service.TravellerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/index")
@RequiredArgsConstructor
public class tripAdvisorControl {
    private final TravellerService service;

    @GetMapping("/{id}")
    public TravellerDto getTraveller(@PathVariable Long id){
        return service.getTraveller(id);
    }

    @GetMapping
    public List<Traveller> getAllTravellers(){
        return service.getAllTravellers();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TravellerDto createTraveller(@Valid @RequestBody TravellerRequestDto travellerRequestDto){
        return  service.createTraveller(travellerRequestDto);
    }

    @PutMapping("/{id}")
    public TravellerDto updateTraveller(@PathVariable Long id, @Valid @RequestBody TravellerRequestDto travellerRequestDto){
        return  service.createTraveller(travellerRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTraveller(@PathVariable Long id){
        service.deleteTraveller(id);
    }

}
