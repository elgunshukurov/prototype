package az.ingress.prototype.service;

import az.ingress.prototype.domain.Traveller;
import az.ingress.prototype.dto.TravellerDto;
import az.ingress.prototype.dto.TravellerRequestDto;

import java.util.List;

public interface TravellerService {

    TravellerDto getTraveller(Long id);

    List<Traveller> getAllTravellers();

    TravellerDto createTraveller(TravellerRequestDto travellerRequestDto);

    TravellerDto updateTraveller(Long id, TravellerRequestDto travellerRequestDto);

    void deleteTraveller(Long id);
}
