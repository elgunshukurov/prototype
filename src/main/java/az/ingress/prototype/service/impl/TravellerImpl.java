package az.ingress.prototype.service.impl;

import az.ingress.prototype.domain.Traveller;
import az.ingress.prototype.dto.TravellerDto;
import az.ingress.prototype.dto.TravellerRequestDto;
import az.ingress.prototype.repository.TravellerRepository;
import az.ingress.prototype.service.TravellerService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.print.attribute.standard.Destination;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TravellerImpl implements TravellerService {
    private final TravellerRepository travellerRepository;
    private final ModelMapper modelMapper;

    @Override
    public TravellerDto getTraveller(Long id) {
        if (!travellerRepository.existsById(id)) {
            throw new RuntimeException("Traveller not found!");
        }
        // error while typecasting todo
        TravellerDto travellerDto = (TravellerDto) modelMapper.typeMap(Traveller.class,TravellerDto.class).addMappings(mapping -> {
            mapping.map(Traveller::getId,
                    TravellerDto::setId);
            mapping.map(Traveller::getName,
                    TravellerDto::setName);
            mapping.map(Traveller::getSurname,
                    TravellerDto::setSurname);
            mapping.map(Traveller::getTelephoneNumber,
                    TravellerDto::setTelephoneNumber);
            mapping.map(Traveller::getEmail,
                    TravellerDto::setEmail);
        });
        return travellerDto;
//        return modelMapper.map(travellerRepository.getById(id),TravellerDto.class);
    }

    // todo
    @Override
    public List<Traveller> getAllTravellers() {
        return  travellerRepository.findAll();
    }

    @Override
    public TravellerDto createTraveller(TravellerRequestDto travellerRequestDto) {
        Traveller traveller = modelMapper.map(travellerRequestDto, Traveller.class);
        return modelMapper.map(travellerRepository.save(traveller),TravellerDto.class);
    }

    @Override
    public TravellerDto updateTraveller(Long id, TravellerRequestDto travellerRequestDto) {
        if (!travellerRepository.existsById(id)) {
            throw new RuntimeException("Traveller not found!");
        }
        Traveller traveller = modelMapper.map(travellerRequestDto, Traveller.class);
       return modelMapper.map(travellerRepository.save(traveller),TravellerDto.class);
    }

    @Override
    public void deleteTraveller(Long id) {
        travellerRepository.deleteById(id);
    }

    public static void assertEquals(Collection<?> a, Set<?> b) {
        for (Object object : a)
            if (!b.contains(object))
                throw new AssertionError();
    }

}
