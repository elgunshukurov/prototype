package az.ingress.prototype.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Data
//@Component
public class TravellerRequestDto {
    @NotBlank
    private String name;

    @NotBlank
    private String surname;


    private String telephoneNumber;

    private String email;

}
