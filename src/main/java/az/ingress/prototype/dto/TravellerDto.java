package az.ingress.prototype.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
//@Component
public class TravellerDto {
    private long id;
    private String name;
    private String surname;
    private String telephoneNumber;
    private String email;
}
