package az.ingress.prototype.repository;

import az.ingress.prototype.domain.Traveller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


//@Repository
public interface TravellerRepository extends JpaRepository<Traveller, Long> {

}
