package az.ingress.prototype.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Traveller {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String surname;

    private String telephoneNumber;

    private String email;

    //comment in bank-config
}
